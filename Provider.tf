provider "aws" {
region = var.AWS_REGION
access_key = "AKXXXXX889XXXX999X”
secret_key = "vPXXXXXXXXXXXXXXWXXXvXXXXXXt"
}
resource "aws_instance" “webserverec2" {
ami= "ami-0e472ba40eb589f49"
instance_type = "t2.micro"
}
#Create security group with firewall rules
resource "aws_security_group" "security_server_port" {
  name        = "security_webserver_port"
  description = "security group for web server”

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

 # outbound from jenkis server
  egress {
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags= {
    Name = "security_webserver_port"
  }
}
	
